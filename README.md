# OCR Software For Malayalam 

<p>This is a Malayalam OCR Software. OCR takes in a document image and renders the text in the image. This OCR reads and transcripts Malayalam and English.</p>

## Libraries Used

NodeJS : Electron, zerorpc. 
<br>
Opencv.js is also used, without integrating it with node.
<br>
Python3: numpy, Keras, Pillow.

### Steps for Execution
Clone this repo.
#### For Python3 Part
 * Create a virtual environment for python3.
 * Install the above said python packages.
#### For NodeJS Part
 * Run the installation by setting up ```npm install```.
 * Change Line-32 and Line-38 in `main.js` to suit your path to `api.py` and python interpreter.
 * Run ```npn start``` for starting the OCR application.


##### In case of Node version error:
 * ```npm install electron-rebuild && ./node_modules/.bin/electron-rebuild```

### Authors:
 * Deepu Shaji, Research Assistant ICFOSS






