# SRR OCR
# Created and Maintained by Deepu Shaji @ ICFOSS <deepushaji@icfoss.in>
import io
import zerorpc
import numpy as np
import os
from PIL import Image
import base64
import format_creator
from keras.models import load_model

main_list = {}

main_list["mal_model"] = 'ംഃഅആഇഈഉഊഋഌഎഏഐഒഓഔകഖഗഘങചഛജഝഞടഠഡഢണതഥദധനഩപഫബഭമയരറലളഴവശഷസഹാിീുൂൃെേൈൊോൌ്ൎൗൺൻർൽൾ'

main_list["mal_ascii_model"] = 'ംഃഅആഇഈഉഊഋഌഎഏഐഒഓഔകഖഗഘങചഛജഝഞടഠഡഢണതഥദധനഩപഫബഭമയരറലളഴവശഷസഹാിീുൂൃെേൈൊോൌ്ൎൗൺൻർൽൾ'+'-+=!@#$%^&*(){}[]|\'"\\,./?<>;:0123456789'

main_list["eng_model"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

class CalcApi(object):
    def recognition(self, array):
        try:
            k = 'some error'  
            text = array[0]
            contour_arrays = array[2]
            thresh = array[3]
            imgdata = base64.b64decode(text[22:])
            img = Image.open(io.BytesIO(imgdata))
            char_list = main_list[array[1]]
            rgba = np.array(img)
            gray = np.dot(rgba[...,:3], [0.299, 0.587, 0.114]) #gray scaling using numpy
            k = format_creator.run_recognition(contour_arrays,gray,thresh,model,char_list,array[1])
            return str(k)
        except Exception as e:
            return str(e)
    def change_model(self,tex):
            global model
            model = load_model(os.path.dirname(os.path.abspath(__file__))+'/'+tex+'.h5') #change models for mal, mal-ascii, eng
            return True
    def echo(self,v):
            return 'server ready'

def parse_port():
    port = 4242
    try:
        port = 4242
    except Exception as e:
        pass
    return '{}'.format(port)

def main():
    addr = 'tcp://127.0.0.1:' + parse_port() #initiating the zerorpc server on the backend side
    s = zerorpc.Server(CalcApi())
    s.bind(addr)
    print('start running on {}'.format(addr))
    s.run()

if __name__ == '__main__':
    main()
