const {app, BrowserWindow} = require('electron')
const path = require('path')



const PY_DIST_FOLDER = 'ocrdist'
const PY_FOLDER = 'ocr'
const PY_MODULE = 'api' // without .py suffix

let pyProc = null
let pyPort = null

const guessPackaged = () => {
  const fullPath = path.join(__dirname, PY_DIST_FOLDER)
  return require('fs').existsSync(fullPath)
}

const getScriptPath = () => {
  if (guessPackaged()) {
    return path.join(__dirname, PY_DIST_FOLDER, PY_MODULE, 'api')
  }else{
    return path.join(__dirname, PY_DIST_FOLDER)
	}
}

const selectPort = () => {
  pyPort = 4242
  return pyPort
}

const createPyProc = () => {
  let script = '/home/icfoss/Desktop/Projects/git_repos/ocr-tool/ocr/api.py' //change this settings path to your computer path
  let port = '' + selectPort()

  if (guessPackaged()) {
    pyProc = require('child_process').execFile(script, [port]) // if packaged, the module will run as a executable process instead of running it through the virtual environment.
  } else {
    pyProc = require('child_process').spawn('/home/icfoss/ocr/bin/python', [script, port]) // direct to virtual environment
  }
}

const exitPyProc = () => {
  pyProc.kill()
  pyProc = null
  pyPort = null
}

app.on('ready', createPyProc)
app.on('will-quit', exitPyProc)


//_________________________________________________________________________________________________________________

let mainWindow

function createWindow () {
  var loc = 'file://'+__dirname+'/'
  mainWindow = new BrowserWindow({icon:'favicon.png'})
  mainWindow.maximize()

  mainWindow.loadURL(loc+'index.html')
  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', createWindow)


app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})
